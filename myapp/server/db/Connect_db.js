var mysql = require('mysql');
var connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  port: 3306,
  database: "svt"
});

connection.connect(function(err) {
    if (err) throw err;
});

module.exports = connection;
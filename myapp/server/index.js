/**
 * Author: Andreas Poulsen
 * Project: Assignment from SVT
 * Desciption: Creates a table with entries if no table allready exsist in a mysql db. Provides CRUD functions and express interface for user too use db.
 */

/****************************/
/* IMPORT MODULES AND SETUP */
/****************************/
const express = require("express");         // front end library for Node.js
const bodyParser = require("body-parser");  // for sending and recieving data with json for the whole application
const path = require('path');               // use path functionality to provide front end website

//import express from 'express';          // front end library for Node.js
//import bodyParser from 'body-parser';   // for sending and recieving data with json for the whole application

const app = express();
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }));

/***********************/
/* IMPORT OWN JS FILES */
/***********************/
const usersRoutes = require('./routes/users.js');
const con = require('./db/Connect_db.js'); //Connecting to db
  
async function Init() {
    //con = await db; //Connecting to db
    console.log('Message:', con);
    db_functions();
}

/*************/
// FRONTPAGE //
/*************/
// http://localhost:3000 
app.listen(3000, () => console.log('listening on port 3000'), );

app.use(express.static("static")); //Use for css
app.get('/',(req,res)=> res.sendFile(path.join(__dirname,'../static','index.html')) );

/*****************/
/* OWN FUNCTIONS */
/*****************/
function db_functions(){
    app.use('/users', usersRoutes); 
    console.log("TEST FUNCTION! ska komma sist");
}


  
Init(); //Starts the software
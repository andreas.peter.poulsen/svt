/************/
/* AJAX GET */
/************/

function loadAllusers() {
    var req = new XMLHttpRequest();
    req.responseType = 'json';
    req.open('GET', "/users", true);
    
    req.onload  = function() {
        var jsonResponse = req.response;
        if(jsonResponse == null || jsonResponse == "" || jsonResponse == undefined){
            notFound();
        } else{
            createTable(jsonResponse); 
        }
             
    };
    req.send(null);
}

function searchUser() {
    var searchWord = document.getElementById("search_input").value;

    var req = new XMLHttpRequest();

    req.responseType = 'json';
    req.open('GET', "/users/search?key=" + searchWord, true);
    req.setRequestHeader("Content-type","application/x-www-form-urlencoded");

    req.onload = function() {
        if (this.readyState == req.DONE && this.status == 200) {
            var jsonResponse = req.response;
            if(jsonResponse == null || jsonResponse == "" || jsonResponse == undefined){
                notFound();
            } else{
                createTable(jsonResponse); 
            }
        } else {
            console.log('server error');
        }
    };
      
    req.onerror = function() {
        console.log('something went wrong');
    };

    req.send(null);
}

/********************/
/* UPDATE TELEPHONE */
/********************/
function updateTelephone(e) {
    console.log("updateTelephone: " + e.target.value);
    console.log("updateTelephone ID: " + e.target.id);

    var req = new XMLHttpRequest();

    req.open('GET', "/users/updateTelephone?newnumber=" + e.target.value + "&id=" + e.target.id, true);
    req.setRequestHeader("Content-type","application/x-www-form-urlencoded");

    req.onload = function() {
        if (this.readyState == req.DONE && this.status == 200) {
            console.log(req.responseText);
            if(req.responseText = "saved"){
                ToggleCss();
            }
        }
    };
      
    req.onerror = function() {
        console.log('something went wrong');
    };

    req.send(null);
}

/*************/
/* FUNCTIONS */
/*************/

// This function creates a table. 
// It takes a Json object
function createTable(jsonResponse){
    var tableArray = ["<thead><tr><th>Firstname</th><th>Lastname</th><th>Email</th><th>Telephone</th></tr></thead><tbody>"];

    jsonResponse.forEach(element => {
        tableArray.push('<tr><td>' + element.firstname + '</td><td>' + element.lastname + '</td><td>' + element.email + `</td><td><input id="${element.id}" onkeyup="updateTelephone(event)" value="${element.telephone}" ></td></tr>`);
    });
    
    tableArray.push("</tbody>");

    document.getElementById("table").innerHTML = tableArray.join(''); //Join used to remove commas
}

// This function can be used to show an error message when the Database can't find any result
function notFound(){
    document.getElementById("table").innerHTML = "<p>Sorry, we could not find the one you were looking for, please check the spelling and try again.</p>"; //Join used to remove commas
}


// Function too controll saved animation
function ToggleCss(){
    let item = document.getElementById('saveDB');

    item.classList.add("fadeinOut");     // Standard syntax
    item.addEventListener("animationend", myEndFunction);

    function myEndFunction() {
        item.classList.remove("fadeinOut"); 
    }
  
}
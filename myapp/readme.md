Arbetsprov 
========== 
Arbetsprovet ska innehålla källkoden och en beskrivning av ditt tankesätt kring lösningen.  

Målet med arbetsprovet är att ge oss en förståelse för hur du normalt tar dig an en sådan här uppgift.  

Kriterierna vi kommer att använda vid bedömningen är följande: 
  - Tillvägagångssätt 
  - Arkitektur 
  - Databasprinciper 

Du avgör själv hur "djupt" du utför arbetsprovet för att ovanstående kriterier uppfylls. 


Uppgift 
======= 
Uppgiften går ut på att implementera en webbaserad kontaktbok. Varje kontakt i boken ska åtminstone innehålla ett förnamn, efternamn, telefonnummer och en e-postadress. Det ska finnas ett gränssnitt som på ett enkelt sätt kan bytas ut i framtiden. Datan ska hämtas via ett enklare backend. All data ska lagras i en MySQL databas eller motsvarande.

Man ska kunna göra följande i gränssnittet: 
  - lista kontakterna. 
  - uppdatera telefonnummer.
  - söka bland kontakterna. 
 

Du får själv bestämma vilka tekniker du vill använda. 

Vi skickar med en SQL-fil med data som du kan använda dig av. 


Leverans 
======== 
Obs: Glöm inte skicka med databasschemat. 

Leveransen sker antingen via GitHub, BitBucket eller motsvarande.
Skicka länken till repot till svtdesign@svt.se. 

Om du lägger upp systemet på någon molntjänst och använder dig av containers, t.ex. docker så är det ett plus i kanten. Om du har tid får du gärna lägga till animationer på sidan eftersom vi jobbar mycket med rörlig bild och animationer, men detta är inget krav.